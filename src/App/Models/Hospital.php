<?php

namespace Models;

use \Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    protected $table = 'hospitals';
    public $timestamps = false;
    protected $fillable = ['name', 'address', 'phone'];

    public function users()
    {
        return $this->hasMany(User::class, 'workplace_id', 'id');
    }
}
