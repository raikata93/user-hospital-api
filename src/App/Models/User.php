<?php

namespace Models;

use \Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';
    public $timestamps = false;
    protected $fillable = ['email', 'first_name', 'last_name', 'type', 'workplace_id', 'created_at'];

    public function hospital()
    {
        return $this->hasOne(Hospital::class, 'workplace_id');
    }
}
