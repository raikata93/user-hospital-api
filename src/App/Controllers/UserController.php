<?php
namespace Controllers;
require 'bootstrap.php';

use Models\Hospital;
use Models\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class UserController {

    public function all() {
        $response = new JsonResponse();
        $users = User::all();
        $response->setData(["code"=>Response::HTTP_OK, "body" => $users]);

        return $response;
    }
    public function create(Request $request) {
        $response = new JsonResponse();

        $typeUsers = ['doctor', 'patient'];
        if ($request->getContentTypeFormat() != 'json') {
            $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                'error' => 'This is not a valid json'
            ]]);
            return $response;
        }

        $data = json_decode($request->getContent(), true);
        if (empty($data)) {
            $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                'error' => 'No data provided'
            ]]);
            return $response;
        }

        $hospital = Hospital::where("id",$data["workplace_id"])->first();
        if (is_null($hospital)) {
            $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                'error' => 'Hospital not exists'
            ]]);
            return $response;
        }

        if (!in_array($data['type'], $typeUsers)) {
            $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                'error' => 'Not valid type of user'
            ]]);
            return $response;
        }

        User::create(["email" => $data["email"], "first_name" => $data['first_name'], "last_name" => $data["last_name"], "type" => $data["type"], "workplace_id"=> $data["workplace_id"], "created_at"=>date('Y-m-d H:i:s')]);
        $response->setData(["code" => Response::HTTP_CREATED, "body" => [
            'message' => 'Successfully created User'
        ]]);

        return $response;
    }

    public function delete($id) {
        $response = new JsonResponse();
        $user = User::where("id", $id)->first();
        if ($user == null) {
            $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                'error' => 'User not exists'
            ]]);
            return $response;
        }
        $user->delete();
        $response->setData(["code" => Response::HTTP_OK, "body"=> [
            'message' => 'Successfully deleted user'
        ]]);

        return $response;
    }

    public function getUser($id) {
        $response = new JsonResponse();
        $user = User::where("id", $id)->first();
        if ($user == null) {
            $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                'error' => 'User not exists'
            ]]);
            return $response;
        }

        $response->setData(["code" => Response::HTTP_OK, "body" => $user]);

        return $response;
    }


    public function update(Request $request, $id) {
        $response = new JsonResponse();
        $user = User::where("id", $id)->first();
        if ($user == null) {
            $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                'error' => 'User not exists'
            ]]);
            return $response;
        }

        if ($request->getContentTypeFormat() != 'json') {
            $response->setData(["code"=>Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                'error' => 'This is not a valid json'
            ]]);
            return $response;
        }

        $data = json_decode($request->getContent(), true);
        if (empty($data)) {
            $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                'error' => 'No data provided'
            ]]);
            return $response;
        }

        $hospital = Hospital::where("id",$data["workplace_id"])->first();
        if (is_null($hospital)) {
            $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                'error' => 'Hospital not exists'
            ]]);
            return $response;
        }

        $user->update($data);
        $response->setData(["code" => Response::HTTP_OK, "body" => [
            'message' => 'Successfully updated user'
        ]]);

        return $response;
    }

        public function search(Request $request) {
            $response = new JsonResponse();
            if ($request->getContentTypeFormat() != 'json') {
                $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                    'error' => 'This is not a valid json'
                ]]);
                return $response;
            }
            $data = json_decode($request->getContent(), true);
            if (empty($data)) {
                $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                    'error' => 'No data provided'
                ]]);
                return $response;
            }
            $hospital = Hospital::where("name", $data['hospital'])->first();
            if (is_null($hospital)) {
                $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                    'error' => 'Hospital not exists'
                ]]);
                return $response;
            }

            $users = User::where("workplace_id",$hospital->id)->get();
            if ($users->count() == 0) {
                $response->setData(["code" => Response::HTTP_OK, "body" => [
                    'message' => 'No workers at this hospital'
                ]]);
                return $response;
            }
            $response->setData(["code" => Response::HTTP_OK, "body" => $users]);

            return $response;
        }
}