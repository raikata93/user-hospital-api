<?php
namespace Controllers;
require 'bootstrap.php';

use Models\Hospital;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HospitalController {
    public function all() {
        $response = new JsonResponse();
        $hospitals = Hospital::all();
        $response->setData(["code"=>Response::HTTP_OK, "body"=> $hospitals]);

        return $response;
    }

    public function create(Request $request) {
        $response = new JsonResponse();
        if ($request->getContentTypeFormat() != 'json') {
            $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                'error' => 'This is not a valid json'
            ]]);
            return $response;
        }

        $data = json_decode($request->getContent(), true);
        if (empty($data)) {
            $response->setData(["code" => Response::HTTP_UNPROCESSABLE_ENTITY, "body" => [
                'error' => 'No data provided'
            ]]);
            return $response;
        }


        Hospital::create(["name" => $data["name"], "address" => $data['address'], "phone" => $data["phone"]]);
        $response->setData(["code" => Response::HTTP_CREATED, "body" => [
            'message' => 'Successfully created hospital'
        ]]);

        return $response;
    }

    public function delete($id) {
        $response = new JsonResponse();
        $hospital = Hospital::where("id", $id)->first();
        if ($hospital == null) {
            $response->setData(["code"=>Response::HTTP_UNPROCESSABLE_ENTITY, "body"=> [
                'error' => 'Hospital not exists'
            ]]);
            return $response;
        }
        $hospital->delete();
        $response->setData(["code"=>Response::HTTP_OK, "body"=>[
            'message' => 'Successfully deleted hospital'
        ]]);

        return $response;
    }

    public function getHospital($id) {
        $response = new JsonResponse();
        $hospital = Hospital::where("id", $id)->first();
        if ($hospital == null) {
            $response->setData(["code"=>Response::HTTP_UNPROCESSABLE_ENTITY, "body"=> [
                'error' => 'Hospital not exists'
            ]]);
            return $response;
        }
        $response->setData(["code"=>Response::HTTP_OK, "body"=> $hospital]);

        return $response;
    }

    public function update(Request $request, $id) {
        $response = new JsonResponse();
        $hospital = Hospital::where("id", $id)->first();
        if ($hospital == null) {
            $response->setData(["code"=>Response::HTTP_UNPROCESSABLE_ENTITY, "body"=> [
                'error' => 'Hospital not exists'
            ]]);
            return $response;
        }

        if ($request->getContentTypeFormat() != 'json') {
            $response->setData(["code"=>Response::HTTP_UNPROCESSABLE_ENTITY, "body"=> [
                'error' => 'This is not a valid json'
            ]]);
            return $response;
        }

        $data = json_decode($request->getContent(), true);
        if (empty($data)) {
            $response->setData(["code"=>Response::HTTP_UNPROCESSABLE_ENTITY, "body"=> [
                'error' => 'No data provided'
            ]]);
            return $response;
        }

        $hospital->update($data);
        $response->setData(["code"=>Response::HTTP_OK, "body"=>[
            'message' => 'Successfully updated hospital'
        ]]);

        return $response;
    }

    public function orderBy($order) {
        $response = new JsonResponse();
        $orderTypes = ['asc', 'desc'];
        if (!in_array(strtolower($order), $orderTypes) || !is_string($order)) {
            $response->setData(["code"=>Response::HTTP_UNPROCESSABLE_ENTITY, "body"=> [
                'error' => 'Invalid order type, please use asc or desc as a url parameter'
            ]]);
            return $response;
        }
        $hospitals = Hospital::withCount("users")->orderBy("users_count", $order)->get();
        $response->setData(["code"=>Response::HTTP_OK, "body"=> $hospitals]);

        return $response;
    }


}
