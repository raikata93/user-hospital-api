<?php
$loader = require './vendor/autoload.php';
$loader->register();

use Controllers\UserController;
use Controllers\HospitalController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

try {
    $users_all = new Route(
        '/users/all',
        array('controller' => UserController::class, 'method' => 'all')
    );

    $hospitals_all = new Route(
        '/hospitals/all',
        array('controller' => HospitalController::class, 'method' => 'all')
    );

    $users_create = new Route(
        '/users/create',
        array('controller' => UserController::class, 'method' => 'create')
    );

    $hospitals_create = new Route(
        '/hospitals/create',
        array('controller' => HospitalController::class, 'method' => 'create')
    );

    $users_delete = new Route(
        '/users/{id}/delete',
        array('controller' => UserController::class, 'method' => 'delete'),
        array('id' => '[0-9]+')
    );

    $hospitals_delete = new Route(
        '/hospitals/{id}/delete',
        array('controller' => HospitalController::class, 'method' => 'delete'),
        array('id' => '[0-9]+')
    );

    $users_get = new Route(
        '/users/{id}/get',
        array('controller' => UserController::class, 'method' => 'getUser'),
        array('id' => '[0-9]+')
    );

    $hospitals_get = new Route(
        '/hospitals/{id}/get',
        array('controller' => HospitalController::class, 'method' => 'getHospital'),
        array('id' => '[0-9]+')
    );

    $users_update = new Route(
        '/users/{id}/update',
        array('controller' => UserController::class, 'method' => 'update'),
        array('id' => '[0-9]+')
    );

    $hospitals_update = new Route(
        '/hospitals/{id}/update',
        array('controller' => HospitalController::class, 'method' => 'update'),
        array('id' => '[0-9]+')
    );

    $hospitals_order = new Route(
        '/hospitals/{order}/order',
        array('controller' => HospitalController::class, 'method' => 'orderBy')
    );

    $users_search = new Route(
        '/users/search',
        array('controller' => UserController::class, 'method' => 'search')
    );

    $users_all->setMethods(["GET"]);
    $hospitals_all->setMethods(["GET"]);
    $users_create->setMethods(["POST"]);
    $hospitals_create->setMethods(["POST"]);
    $users_delete->setMethods(["GET"]);
    $hospitals_delete->setMethods(["GET"]);
    $users_get->setMethods(["GET"]);
    $hospitals_get->setMethods(["GET"]);
    $users_update->setMethods(["POST"]);
    $hospitals_update->setMethods(["POST"]);
    $hospitals_order->setMethods(["GET"]);
    $users_search->setMethods(["POST"]);

    $routes = new RouteCollection();
    $routes->add('users.all', $users_all);
    $routes->add('hospitals.all', $hospitals_all);
    $routes->add('users.create', $users_create);
    $routes->add('hospitals.create', $hospitals_create);
    $routes->add('users.delete', $users_delete);
    $routes->add('hospitals.delete', $hospitals_delete);
    $routes->add('users.get', $users_get);
    $routes->add('hospitals.get', $hospitals_get);
    $routes->add('users.update', $users_update);
    $routes->add('hospitals.update', $hospitals_update);
    $routes->add('hospitals.order', $hospitals_order);
    $routes->add('users.search', $users_search);

    $requestContext = new RequestContext();
    $request = Request::createFromGlobals();
    $requestContext->fromRequest($request);

    $matcher = new UrlMatcher($routes, $requestContext);
    $attributes = $matcher->match($requestContext->getPathInfo());

    $refMeth = new ReflectionMethod($attributes['controller'].'::'.$attributes['method']);
    foreach($refMeth->getParameters() as $param) {
        $sigParams[] = $param->getName();
    }
    if($sigParams != null && in_array("request", $sigParams)) {
        $attributes['request'] = $request;
    }

    $controller = new $attributes['controller'];
    $method = $attributes['method'];
    unset($attributes['controller']);
    unset($attributes['method']);
    unset($attributes['_route']);

    call_user_func_array([$controller, $method], $attributes)->send();
} catch (ResourceNotFoundException $e) {
    $response = new JsonResponse();
    $response->setData(["code"=>Response::HTTP_NOT_FOUND, "body"=> json_encode([
        'error' => 'Resource not found'
    ])]);

    return $response;
} catch(Symfony\Component\Routing\Exception\MethodNotAllowedException $e) {
    $response = new JsonResponse();
    $response->setData(["code"=>Response::HTTP_METHOD_NOT_ALLOWED, "body"=> json_encode([
        'error' => 'Method not allowed'
    ])]);

    return $response;
}