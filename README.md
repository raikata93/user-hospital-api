# User Hospital API

https://github.com/credoweb/recruitment/blob/master/developer-api-task.md


## Setup

1. Install [composer](https://getcomposer.org/download/)
2. Setup database and fill config/database.php
1. Run `composer install` in order to install all packages which are need to API
1. Run `vendor/bin/phinx init` in order to init phinx env which we will use for DB migrations. I attached the phinx documents below.
1. Go to `phinx.php` file and fill database config data in one of the envs. I'm using development
1. Run `vendor/bin/phinx migrate -e development`, I filled development env with needed config data. With this we will merge all migrates to you database
2. Run `vendor/bin/phinx seed:run` in order to seed some test data in both tables.
3. Run `php -S localhost:8000` , in order to make access to API. Of course you can use every port which you want

## Phix documentation
https://book.cakephp.org/phinx/0/en/index.html

## API DOCS

### Users
#### Get all users from DB GET
    /users/all
#### Create user POST
    /users/create 
Request Body:
****
    {
        "email": "momchil@abv.bg",
        "first_name": "Momchi",
        "last_name":"Gospodinov",
        "type":"doctor",
        "workplace_id":2
    }
#### Delete user  GET
    /users/{id}/delete
`{id}` is the id of existing user, if you set id of not existing user you will receive error message

#### Get single user by id GET
    /users/{id}/get
`{id}` is the id of existing user, if you set id of not existing user you will receive error message

#### Update user POST
    /users/{id}/update 
`{id}` is the id of existing user, if you set id of not existing user you will receive error message
Request Body:
****
    {
        "email": "momchil@abv.bg",
        "first_name": "Momchi",
        "last_name":"Gospodinov",
        "type":"patient",
        "workplace_id":2
    }

#### Search for users working on specific hospital POST
    /users/search 
****
    {
        "hospital": "Tokuda",
    }
Based on the data which will come from request body it will return the users who work there. If you add hospital which not exists it will return error message


### Hospitals
#### Get all hospitals from DB GET
    /hospitals/all
#### Create hospital POST
    /hospitals/create 
Request Body:
****
    {
        "name": "Tokuda",
        "address": "Sofiq",
        "phone":"95847152"
    }
#### Delete hospital  GET
    /hospitals/{id}/delete
`{id}` is the id of existing hospital, if you set id of not existing hospital you will receive error message

#### Get single hospital by id GET
    /hospitals/{id}/get
`{id}` is the id of existing hospital, if you set id of not existing hospital you will receive error message

#### Update hospital POST
    /hospitals/{id}/update 
`{id}` is the id of existing hospital, if you set id of not existing hospital you will receive error message
Request Body:
****
    {
       "name": "Tokuda123",
        "address": "Montana",
        "phone":"1111"
    }

#### Order hospital asc/desc based on the number of employees who work there
    /hospitals/{order}/order
`{order}` it can be `asc` or `desc` if you set different value you will receive error message as a response



Martin Raychev Ivanov
