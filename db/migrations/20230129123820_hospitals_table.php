<?php

use Phinx\Migration\AbstractMigration;

final class HospitalsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $hospitals = $this->table('hospitals');
        $hospitals->addColumn('name', 'string')
            ->addColumn('address', 'string')
            ->addColumn('phone', 'string')
            ->save();
    }

    public function down()
    {
        $this->table('hospitals')->drop()->save();
    }
}
