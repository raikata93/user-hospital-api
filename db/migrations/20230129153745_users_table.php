<?php

use Phinx\Migration\AbstractMigration;

final class UsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $users = $this->table('users');
        $users->addColumn('email', 'string')
            ->addColumn('first_name', 'string')
            ->addColumn('last_name', 'string')
            ->addColumn('type', 'enum', ['values' => ['patient', 'doctor']])
            ->addColumn('workplace_id', 'integer', ['null' => true, 'signed' => FALSE])
            ->addForeignKey('workplace_id', 'hospitals', 'id', ['delete'=> 'CASCADE', 'update'=> 'RESTRICT'])
            ->addColumn('created_at', 'datetime')
            ->save();
    }

    public function down()
    {
        $this->table('users')->drop()->save();
    }
}
