<?php


use Phinx\Seed\AbstractSeed;

class HospitalsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */

    public function run(): void
    {
        $data = [
            [
                'id'    => 1,
                'name' => "tokuda",
                'address' => "nikola vaptsavor",
                'phone' => "02345432",
            ],[
                'id'    => 2,
                'name' => "sveta anna",
                'address' => "tsarigrtsko shose",
                'phone' => "025543677",
            ],[
                'id'    => 3,
                'name' => "sveta sofiq",
                'address' => "bul bulgaria",
                'phone' => "02888964"
            ],[
                'id'    => 4,
                'name' => "sofiq med",
                'address' => "gm dimitrov",
                'phone' => "026654366",
            ],
        ];

        $hospitals = $this->table('hospitals');
        $hospitals->insert($data)->saveData();
    }
}
