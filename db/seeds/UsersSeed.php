<?php


use Phinx\Seed\AbstractSeed;

class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run(): void
    {
        $data = [
            [
                'id'    => 1,
                'email' => "martin@abv.bg",
                'first_name' => "Martin",
                'last_name' => "Ivanov",
                'type' => "patient",
                'workplace_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'id'    => 2,
                'email' => "ivan@abv.bg",
                'first_name' => "Ivan",
                'last_name' => "Ivanov",
                'type' => "doctor",
                'workplace_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'id'    => 3,
                'email' => "dimityr@abv.bg",
                'first_name' => "Dimityr",
                'last_name' => "Dimitrov",
                'type' => "patient",
                'workplace_id' => 2,
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'id'    => 4,
                'email' => "joro@abv.bg",
                'first_name' => "Joro",
                'last_name' => "Dimitrov",
                'type' => "doctor",
                'workplace_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'id'    => 5,
                'email' => "vanq@abv.bg",
                'first_name' => "Vanq",
                'last_name' => "Ivanov",
                'type' => "patient",
                'workplace_id' => 4,
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'id'    => 6,
                'email' => "stoyo@abv.bg",
                'first_name' => "Stoyo",
                'last_name' => "Genev",
                'type' => "doctor",
                'workplace_id' => 2,
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'id'    => 7,
                'email' => "Iliyana@abv.bg",
                'first_name' => "Iliyana",
                'last_name' => "Ivanova",
                'type' => "doctor",
                'workplace_id' => 2,
                'created_at' => date('Y-m-d H:i:s'),
            ],[
                'id'    => 8,
                'email' => "sasho@abv.bg",
                'first_name' => "Alex",
                'last_name' => "Dimitrov",
                'type' => "patient",
                'workplace_id' => 3,
                'created_at' => date('Y-m-d H:i:s'),
            ],
        ];

        $users = $this->table('users');
        $users->insert($data)->saveData();
    }
}
